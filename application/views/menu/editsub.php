<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>

<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
                <form action="" method="post">
                    <input type="hidden" name="id" value="<?= $sub['id']; ?>">
                    <div class="form-group">
                        <label for="menu_id">Menu ID =<br> (1 = admin, 2 = user, 3 = menu)</label>
                        <input type="text" class="form-control" id="menu_id" name="menu_id" value="<?= $sub['menu_id'];?>">
                        <small  class="form-text text-danger"><?= form_error('menu_id'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="title">Title =</label>
                        <input type="text" class="form-control" id="title" name="title" value="<?= $sub['title'];?>">
                        <small  class="form-text text-danger"><?= form_error('title'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="url">Url =</label>
                        <input type="text" class="form-control" id="url" name="url" value="<?= $sub['url'];?>">
                        <small  class="form-text text-danger"><?= form_error('url'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="icon">Icon =</label>
                        <input type="text" class="form-control" id="icon" name="icon" value="<?= $sub['icon'];?>">
                        <small  class="form-text text-danger"><?= form_error('icon'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="is_active">Active =<br> (1 = active, 2 = no active)</label>
                        <input type="text" class="form-control" id="is_active" name="is_active" value="<?= $sub['is_active'];?>">
                        <small  class="form-text text-danger"><?= form_error('is_active'); ?></small>
                    </div>
                    <button type="submit"  class="btn btn-success float-right">edit</button>
                    <a href="<?= base_url('menu/submenu'); ?>" class="btn btn-primary float-right ml-1 mr-1">back</a>
                </form>
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


