<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Home Page</title>
  </head>
  <body>
    
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">OSIS SMA 1 CIBITUNG</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active mr-3">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="<?= base_url('auth/login'); ?>">Login <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="btn btn-outline-success my-2 my-sm-0" href="<?= base_url('auth/registration'); ?>">Registration <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>

    <!--content-->
    <div class="container mt-5">
        <div class="row">
        
            <div class="col-lg-8 mx-auto">
            <?= $this->session->flashdata('message'); ?>
            <h2 class="text-center mb-5">DATA ALUMNI </h2>

            <table class="table table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">OSIS Period</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($alumni as $a) : ?>
                    <tr>
                    <th scope="row"><?= $i; ?></th>
                    <td><?= $a['name']; ?></td>
                    <td><?= $a['email']; ?></td>
                    <td><?= $a['period']; ?></td>
                    <td>
                    <button data-toggle="modal" data-target="#formModal<?=$a['id']; ?>" class="btn btn-success">detail</button>
                    </td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php $no=0; foreach($alumni as $a): $no++; ?>
    <div class="modal fade" id="formModal<?=$a['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModalLabel">Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('home'); ?>" method="post">
      <input type="hidden" name="id" id="id" value="<?=$a['id']; ?>">
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="<?=$a['name']; ?>" placeholder="... name ...">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">OSIS Period</label>
                <input type="text" class="form-control" id="period" name="period" value="<?=$a['period']; ?>" placeholder="... period ...">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">University or Institute</label>
                <input type="text" class="form-control" id="school" name="school" value="<?=$a['school']; ?>" placeholder="... school ...">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Major</label>
                <input type="text" class="form-control" id="major" name="major" value="<?=$a['major']; ?>" placeholder="... major ...">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Company</label>
                <input type="text" class="form-control" id="company" name="company" value="<?=$a['company']; ?>" placeholder="... company ...">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach; ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
</body>
</html>