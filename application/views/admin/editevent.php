<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>

<hr>
<h4 class="text-center"> Edit Event </h4>
<hr>
<div class="container">
    <input type="hidden" name="id" value="<?= $event['id']; ?>">
    <form action="" method="post">
        <div class="row mt-3">
            <div class="col-md-4">
                <div class="form-group">
                    <img src="<?= base_url('assets/img/event/') . $event['image']; ?>" class="img-thumbnail">
                </div>
                <div class="custom-file mt-2">
                    <input type="file" class="custom-file-input" id="image" name="image">
                    <label class="custom-file-label" for="image">Choose File</label>
                </div>
                <div class="form-group mt-3">
                    <button type="submit"  class="btn btn-success float-right">edit</button>
                    <a href="<?= base_url('admin/event'); ?>" class="btn btn-primary float-right ml-1 mr-1">back</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="text">Event Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $event['name'];?>" readonly>
                    <small  class="form-text text-danger"><?= form_error('name'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Theme</label>
                    <input type="text" class="form-control" id="theme" name="theme" value="<?= $event['theme'];?>">
                    <small  class="form-text text-danger"><?= form_error('theme'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Location</label>
                    <input type="text" class="form-control" id="location" name="location" value="<?= $event['location'];?>">
                    <small  class="form-text text-danger"><?= form_error('location'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Information</label>
                    <textarea type="text" class="form-control" id="information" name="information"><?= $event['information'];?></textarea>
                    <small  class="form-text text-danger"><?= form_error('information'); ?></small>
                </div>
            </div>
        </div>
    </form>
</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->