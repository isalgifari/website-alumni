<?php

function check_login()
{
    //menginstansiasi agar helper ini dikenal codeigniter
    $ci = get_instance();

    //tidak memakai $this krn helper ini tidak mengenalnya
    //krn ini helper bukan bawaan codeigniter
    if(!$ci->session->userdata('email')){
        redirect('auth');
    } else {

        //cek role_id di session yang sudah di buat di controller auth/_login
        $role_id = $ci->session->userdata('role_id');

        //uri adalah pengaturan untuk url dari codeigniter
        //segment adalah mengatur url yang mana menjadi patokan
        $menu = $ci->uri->segment(1);

        $quryMenu = $ci->db->get_where('user_menu', ['menu' => $menu])->row_array();

        $menu_id = $quryMenu['id'];

        $userAccess = $ci->db->get_where('user_access_menu', [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ]);

        if($userAccess->num_rows() < 1){
            redirect('auth/blocked');
        }

    }
}

function check_access($role_id, $menu_id)
{
    $ci = get_instance();

    $ci->db->where('role_id', $role_id);
    $ci->db->where('menu_id', $menu_id);
    $result = $ci->db->get('user_access_menu');

    if($result->num_rows() > 0) {
        return "checked='checked'";
    }


     
}

