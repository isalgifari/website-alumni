<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->email();

        $data['alumni'] = $this->db->get('user')->result_array();
        
        $data['title'] = 'Data Alumni';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('alumni/index', $data);
        $this->load->view('templates/footer');
    }
    
    public function event()
    {
        $data['user'] = $this->Auth_model->email();

        $data['event'] = $this->db->get('event')->result_array();
        
        $data['title'] = 'School Event';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('alumni/event', $data);
        $this->load->view('templates/footer');
    }
}
