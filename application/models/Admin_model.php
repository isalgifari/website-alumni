<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function getUserById($id)
    {
        return $this->db->get_where('user', ['id' => $id])->row_array();
    }
    
    public function deleteuser($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user');
    }



    /////////////   EVENT   ////////////////////////
    public function tambahEvent()
    {
        $data = [
            'name' => htmlspecialchars($this->input->post('name', true)),
            'location' => htmlspecialchars($this->input->post('location', true)),
            'date' => htmlspecialchars($this->input->post('date', true)),
            'theme' => htmlspecialchars($this->input->post('theme', true)),
            'information' => htmlspecialchars($this->input->post('information', true)),
            'image' => 'event.jpg'
       ];

       return $this->db->insert('event', $data);
    }

    public function deleteevent($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('event');
    }

    public function getEventById($id)
    {
        return $this->db->get_where('event', ['id' => $id])->row_array();
    }

}